var fs = require('fs');
var readline = require('readline');
var google = require('googleapis');
var googleAuth = require('google-auth-library');

let uuidV1 = require('uuid/v1');

// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/drive-nodejs-flexfi.json
var SCOPES = ['https://www.googleapis.com/auth/drive'];
var TOKEN_DIR = (process.env.HOME || process.env.HOMEPATH ||
	process.env.USERPROFILE) + '/.credentials/';
var TOKEN_PATH = TOKEN_DIR + 'drive-nodejs-flexfi.json';

// common variables
let watchFiles = [];


/* For web server - start */

/* data declarations */

// constants
const
	// expressjs server
	express = require("express"),
	bodyParser = require("body-parser"),
	// HTTPS certificate
	pkey = fs.readFileSync("privkey.pem"),
	cert = fs.readFileSync("cert.pem"),
	ca = fs.readFileSync("chain.pem"),
	// ways the process can get terminated
	stopSignals = [
		"SIGHUP", "SIGINT", "SIGQUIT", "SIGILL", "SIGTRAP", "SIGABRT",
		"SIGBUS", "SIGFPE", "SIGUSR1", "SIGSEGV", "SIGUSR2", "SIGTERM"
	];

// connection options
let httpsOptions = {
	key: pkey,
	cert: cert,
	ca: ca
}
let nodePort = 9443;


/* server initialization */

// expressjs server
let server = express();
// to parse request body
server.use(bodyParser.json());

// server configuration
let https = require("https").createServer(httpsOptions, server);
// listen on a port
https.listen(nodePort, function () {
	console.log("Listening on port " + nodePort);
});

// report server health
server.get("/health", function (req, res) {
	res.writeHead(200);
	res.end();
});

// when process is killed
stopSignals.forEach(function (signal) {
	process.on(signal, function () {
		// Stop 'file change' notifications
		fs.readFile('client_secret.json', function processClientSecrets(err, content) {
			if (err) {
				console.log('Error loading client secret file: ' + err);
				return;
			}
			// Authorize a client with the loaded credentials, then call the
			// Drive API.
			authorize(JSON.parse(content), closeFiles);
		});
		// exit the process
		process.exit();
	});
});


/* server requests */

// tracked files
let getFiles = function () {
	return watchFiles;
}

// include submodules
const
	listen = require("./listen");

// export submodules
listen(server, fs, getFiles);

/* For web server - close */


// Load client secrets from a local file.
fs.readFile('client_secret.json', function processClientSecrets(err, content) {
	if (err) {
		console.log('Error loading client secret file: ' + err);
		return;
	}
	// Authorize a client with the loaded credentials, then call the
	// Drive API.
	authorize(JSON.parse(content), trackFiles);
});

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 *
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
	var clientSecret = credentials.installed.client_secret;
	var clientId = credentials.installed.client_id;
	var redirectUrl = credentials.installed.redirect_uris[0];
	var auth = new googleAuth();
	var oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);

	// Check if we have previously stored a token.
	fs.readFile(TOKEN_PATH, function (err, token) {
		if (err) {
			getNewToken(oauth2Client, callback);
		} else {
			oauth2Client.credentials = JSON.parse(token);
			callback(oauth2Client);
		}
	});
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 *
 * @param {google.auth.OAuth2} oauth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback to call with the authorized
 *     client.
 */
function getNewToken(oauth2Client, callback) {
	var authUrl = oauth2Client.generateAuthUrl({
		access_type: 'offline',
		scope: SCOPES
	});
	console.log('Authorize this app by visiting this url: ', authUrl);
	var rl = readline.createInterface({
		input: process.stdin,
		output: process.stdout
	});
	rl.question('Enter the code from that page here: ', function (code) {
		rl.close();
		oauth2Client.getToken(code, function (err, token) {
			if (err) {
				console.log('Error while trying to retrieve access token', err);
				return;
			}
			oauth2Client.credentials = token;
			storeToken(token);
			callback(oauth2Client);
		});
	});
}

/**
 * Store token to disk be used in later program executions.
 *
 * @param {Object} token The token to store to disk.
 */
function storeToken(token) {
	try {
		fs.mkdirSync(TOKEN_DIR);
	} catch (err) {
		if (err.code != 'EEXIST') {
			throw err;
		}
	}
	fs.writeFile(TOKEN_PATH, JSON.stringify(token));
	console.log('Token stored to ' + TOKEN_PATH);
}

/**
 * Specify files to track.
 *
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
function trackFiles(auth) {
	var service = google.drive({ version: 'v3', auth: auth });
	// Get file ID for files to track
	service.files.list({
		q: "name='FlexFi-application.json' or name='FlexFi-credit_report.xml'",
		fields: "nextPageToken, files(id, name)"
	}, function (err, response) {
		if (err) {
			console.log('The API returned an error: ' + err);
			return;
		}
		var inputFiles = response.files;
		if (inputFiles.length == 0) {
			console.log('No files found.');
		} else {
			console.log('Tracking files:');
			// Watch for changes in each file
			for (var i = 0; i < inputFiles.length; i++) {
				service.files.watch({
					fileId: inputFiles[i].id,
					resource: {
						id: uuidV1(),
						type: "web_hook",
						address: "https://www.reninf.com:9443/parseFlexFi",
						token: inputFiles[i].id
					}
				}, function (err, response) {
					// Save resource info
					watchFiles.push(response);
				});
				console.log('%s (%s)', inputFiles[i].name, inputFiles[i].id);
			}
		}
	});
}

/**
 * Stop watching for changes and end notifications.
 *
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
function closeFiles(auth) {
	var service = google.drive({ version: 'v3', auth: auth });
	// Stop notification channels
	for (let i = 0; i < watchFiles.length; i++) {
		service.channels.stop({
			resource: {
				id: watchFiles[i].id,
				resourceId: watchFiles[i].resourceId
			}
		});
	}
}