https://youtu.be/XjYzAod80Zo

This Node.js program reads a JSON file that is dropped in Google Drive and creates a CSV file from it.
(Removed sensitive files from the project by putting them in the .gitignore list).
This was for the develop challenge in VanHack of 2017.
Therefore code is not of production quality.
Please read further for the methodology.

Files (FlexFi-application.json or FlexFi-credit_report.xml), if dropped in Google Drive, are picked up instantly by the Node.js application because it gets notified by Google Drive API.

For this challenge, due to lack of time, I have only written code to convert the JSON file into CSV.
I skipped the XML file from the conversion logic.



Here's a summary on how it works:

1) The program gets the file ID-s of the above files on my Google Drive

2) It asks Google Drive to watch for changes to the file and directs it to inform https://www.reninf.com:9443/parseFlexFi (which was only up during testing) in case of any change.

Please note, since this was a development challenge, I haven't written any code to ensure the "file watch request" is restarted before it expires.

3) The Node.js application starts a web server at the above location to listen for any notifications from Google Drive.

4) When notification about a file change arrives, the program checks the request to see what file was changed and reads it from the Drive.

5) It reads all the nodes in the JSON files dynamically and converts it to CSV format.

6) The CSV file is then written to a hard-coded folder on my Google Drive.


How to run:

In order to use this code for your own Google Drive account, you have to perform the following steps:

1) You have to create a client secret file for your own Google Drive by creating a project in Google Developer's console.

2) In the above project you have to verify your domain with Google.

3) You have to supply the SSL certificate files for your domain.

4) Then run the program with node start.js

5) This program will require a lot of enhancements before it can be used for a productive application.
(For e.g. it will need to specify a longer expiration time or periodically request Google to renew the "file watch request").


The original challenge:

Description
Our challenge is to write a code to parse an XML or JSON file that is stored in a Google Drive and save the transactional data in a csv file, respecting the node structure of the original file. A new file saved on the folder triggers the code. The original file contains sensitive data, like bank statement or credit report.
 
Overview
Both files are stored in Google Drive. For this challenge, we provided 2 test data files, similarly what FlexFi is receiving after the applicant submits the loan application.
 
Files for Testing
 
Bank Statement
Description: Up to 90 transactional data
Folder: Challenge\1.JSON
Name: bank.json
Format: JSON file format
 
Credit Report
Description: Credit report
Folder: Challenge\2.XML
Name: credit_report.xls
Format: XML file format
 
 
https://drive.google.com/open?id=0ByD-G8tCBuktSFpRdGI3Y0hjMkE