var google = require('googleapis');
var googleAuth = require('google-auth-library');

var TOKEN_DIR = (process.env.HOME || process.env.HOMEPATH ||
    process.env.USERPROFILE) + '/.credentials/';
var TOKEN_PATH = TOKEN_DIR + 'drive-nodejs-flexfi.json';

// export listen module
module.exports = function (server, fs, getFiles) {

    let extRequest = null;

    /* parse FlexFi files */
    server.post('/parseFlexFi', function (req, res) {

        // ignore sync message
        if (req.headers["x-goog-resource-state"] != "sync") {
            // check for valid channel id
            let valid = false;
            let watchFiles = getFiles();
            for (let i = 0; i < watchFiles.length; i++) {
                if (watchFiles[i].id === req.headers["x-goog-channel-id"]) {
                    valid = true;
                    break;
                }
            }
            if (valid) {
                extRequest = req;

                // Load client secrets from a local file.
                fs.readFile('client_secret.json', function processClientSecrets(err, content) {
                    if (err) {
                        console.log('Error loading client secret file: ' + err);
                        return;
                    }
                    // Authorize a client with the loaded credentials, then call the
                    // Drive API.
                    authorize(JSON.parse(content), parseFiles);
                });
            }
        }

        res.send("Success");
    });

    /**
    * Parse files on Google Drive.
    *
    * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
    */
    function parseFiles(auth) {
        var service = google.drive({ version: 'v3', auth: auth });

        // Read file contents
        service.files.get({
            fileId: extRequest.headers["x-goog-channel-token"],
            alt: "media"
        }, function (err, response) {
            let csvOutput = "";
            if (typeof response === "object") {
                //JSON file
                let breakDown = function (jsonObject) {
                    for (let key in jsonObject) {
                        // loop each key
                        if (jsonObject[key] !== null && typeof jsonObject[key] === "object") {
                            // if json object or Array
                            if (jsonObject[key].constructor === Array) {
                                csvOutput += "\r\n";
                                // if object is an array
                                for (let i = 0; i < jsonObject[key].length; i++) {
                                    breakDown(jsonObject[key][i]);
                                    csvOutput += "\r\n";
                                }
                            } else {
                                csvOutput += "\r\n" + key + ",";
                                breakDown(jsonObject[key]);
                            }
                        } else {
                            csvOutput += jsonObject[key] + ",";
                        }
                    }
                }
                // start recursive function
                breakDown(response);
                // CSV output
                fs.createWriteStream("FlexFi-application.csv").write(csvOutput, function (err) {
                    if (err) {
                        console.log("Error while writing CSV file");
                    } else {
                        // upload to Google Drive
                        service.files.create({
                            resource: {
                                name: "FlexFi-application.csv",
                                parents: ["0BwysBTo0iLvWbmZiNzRfSWcwT3c"]
                            },
                            media: {
                                mimeType: "text/csv",
                                body: fs.createReadStream("FlexFi-application.csv")
                            },
                            fields: "name"
                        }, function (err, response) {
                            console.log("Created" + response.name);
                        });
                    }
                });
            } else {
                // XML file
                // TODO - later
            }
        });
    }

    /**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 *
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
    function authorize(credentials, callback) {
        var clientSecret = credentials.installed.client_secret;
        var clientId = credentials.installed.client_id;
        var redirectUrl = credentials.installed.redirect_uris[0];
        var auth = new googleAuth();
        var oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);

        // Check if we have previously stored a token.
        fs.readFile(TOKEN_PATH, function (err, token) {
            if (err) {
                // getNewToken(oauth2Client, callback);
            } else {
                oauth2Client.credentials = JSON.parse(token);
                callback(oauth2Client);
            }
        });
    }
}